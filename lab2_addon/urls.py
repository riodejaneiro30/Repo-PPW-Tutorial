from django.conf.urls import url, include
from .views import index

#url for app
urlpatterns = [
    url(r'^$', index, name='index')
]
