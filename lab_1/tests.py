from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, mhs_name, calculate_age
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here.

# <<<<<<< HEAD
# =======
# <<<<<<< HEAD
# >>>>>>> 8b1205889bc93847984d8665bb9edb0b00663137
# class HelloNameUnitTest(TestCase):

#     def test_hello_name_is_exist(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code,200)

#     def test_using_index_func(self):
#         found = resolve('/')
# <<<<<<< HEAD
# =======
# =======
class Lab1UnitTest(TestCase):

    def test_hello_name_is_exist(self):
        response = Client().get('/lab-1/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-1/')
# >>>>>>> ad670ac239dd5b6ab36be3f3f326451cfafdbb7b
# >>>>>>> 8b1205889bc93847984d8665bb9edb0b00663137
        self.assertEqual(found.func, index)

    def test_name_is_changed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>' + mhs_name + '</title>', html_response)
        self.assertIn('<h1>Hello my name is ' + mhs_name + '</h1>', html_response)
        self.assertFalse(len(mhs_name) == 0)

# <<<<<<< HEAD
# =======
# <<<<<<< HEAD
# =======

# >>>>>>> ad670ac239dd5b6ab36be3f3f326451cfafdbb7b
# >>>>>>> 8b1205889bc93847984d8665bb9edb0b00663137
    def test_calculate_age_is_correct(self):
        self.assertEqual(0, calculate_age(date.today().year))
        self.assertEqual(17, calculate_age(2000))
        self.assertEqual(27, calculate_age(1990))

# <<<<<<< HEAD
# =======
# <<<<<<< HEAD
# =======

# >>>>>>> ad670ac239dd5b6ab36be3f3f326451cfafdbb7b
# >>>>>>> 8b1205889bc93847984d8665bb9edb0b00663137
    def test_index_contains_age(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
# <<<<<<< HEAD
        self.assertRegex(html_response, r'<article>I am [1-9]\d+ years old</article>')
 
# =======
# <<<<<<< HEAD
        self.assertRegex(html_response, r'<article>I am [1-9]\d+ years old</article>')
 
# =======
        self.assertRegex(html_response, r'<article>I am [0-9]\d+ years old</article>')
# >>>>>>> ad670ac239dd5b6ab36be3f3f326451cfafdbb7b
# >>>>>>> 8b1205889bc93847984d8665bb9edb0b00663137
